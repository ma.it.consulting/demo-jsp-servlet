package com.demoapp.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.demoapp.servlet.models.Personne;
import com.demoapp.servlet.services.PersonneService;

@WebServlet(name = "personneServletDemo2", value = "/personne-demo2")
public class PersonneServletDemo2 extends HttpServlet {

	private static final long serialVersionUID = 1L;

	private List<Personne> personnes;
	private PersonneService personneService;

	@Override
	public void init() {
		personneService = new PersonneService();
		personnes = personneService.getPersonnes();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		response.setContentType("text/html");
		PrintWriter print = response.getWriter();

		print.println("<html><body>");

		if (request.getParameter("id") != null) {
			int id = Integer.parseInt(request.getParameter("id"));
			Personne personne = personneService.getPersonne(id);
			System.out.println(personne);

			if (personne != null) {

				print.println("<div>");
				print.println(personne.getPrenom() + " " + personne.getNom());
				print.println("</div>");
			} else {
				print.println("<div>");
				print.println("Il n'y a personne !!! ");
				print.println("</div>");
			}

		} else {

			for (Personne p : personnes) {
				print.println("<div>");
				print.println(p.getPrenom() + " " + p.getNom());
				print.println("</div>");
			}
		}
		print.println("</body></html>");
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		response.setContentType("text/html");
		PrintWriter print = response.getWriter();

		print.println("<html><body>");

		if (request.getParameter("nom") != null && request.getParameter("prenom") != null) {

			String nom = request.getParameter("nom");
			String prenom = request.getParameter("prenom");
			Personne personne = new Personne(nom, prenom);

			if (personneService.addPersonne(personne)) {

				print.println("<div> Personne ajout�e " + personne.getId() + "</div>");

			} else {
				print.println("<div> Erreur op�ration </div>");
			}

		}else {
			print.println("<div> Erreur dans les param�tres </div>");
		}

		print.println("</body></html>");

	}

}
