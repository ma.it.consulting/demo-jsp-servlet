package com.demoapp.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class HttpServletRequestExample extends HttpServlet {

	private static final long serialVersionUID = 1L;
	

	protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {

		doPost(req, res);
		
	}

	protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		
		res.setContentType("text/html");
		
		PrintWriter print = res.getWriter();
		
		print.print("<html><body>");
		print.print("<h1> Request local : " + req.getLocale() + "</h1>");
		print.print("<h1> Request Content Lenght : " + req.getContentLength() + "</h1>");
		print.print("<h1> Request Server name : " + req.getServerName() + "</h1>");
		print.print("<h1> Request Content Type : " + req.getContentType() + "</h1>");
		print.print("<h1> Request Context Path : " + req.getContextPath() + "</h1>");
		print.print("<h1> UserName  : " + req.getParameter("UserName") + "</h1>");
		print.print("<h1> Country  : " + req.getParameter("Country") + "</h1>");
		print.print("</body></html>");
	
		
		
	}
	

}
