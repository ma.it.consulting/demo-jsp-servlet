package com.demoapp.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(
		description = "Simple Servlet", 
		urlPatterns = { "/FeedBackServlet" }, 
		initParams = {@WebInitParam(name = "mail", value = "mohamed@utopios.net", description = "Adresse mail")})
public class FeedBackServlet extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	private String adresseMail = null;
	
       
	@Override
	public void init(ServletConfig config)throws ServletException {
		this.adresseMail = config.getInitParameter("mail");
	}
	
	
    public FeedBackServlet() {
        super();
        // TODO Auto-generated constructor stub
    }


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		doPost(request, response);
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		response.setContentType("text/html");
		PrintWriter print = response.getWriter();
		
		print.print("<h2> Le mail initital est :" + adresseMail + "</h2>");
		
		String feedBackParam = request.getParameter("feedBackParam");
		
		print.print("<h2> Le mail initital est :" + adresseMail +  " et le nouveau mail est : "+feedBackParam +"</h2>");
		
		
		
	}

}
