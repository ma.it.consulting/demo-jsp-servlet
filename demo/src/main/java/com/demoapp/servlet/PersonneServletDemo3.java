package com.demoapp.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.demoapp.servlet.models.Personne;
import com.demoapp.servlet.services.PersonneService;

@WebServlet(name = "personneServletDemo3", value = "/personne-demo3")
public class PersonneServletDemo3 extends HttpServlet {
	
	
	private static final long serialVersionUID = 1L;

	private List<Personne> personnes;
	private PersonneService personneService;

	@Override
	public void init() {
		personneService = new PersonneService();
		personnes = personneService.getPersonnes();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

	
		if (request.getParameter("id") != null) {
			int id = Integer.parseInt(request.getParameter("id"));
			Personne personne = personneService.getPersonne(id);
			if (personne != null) {
				
				request.setAttribute("personne", personne);
				request.getRequestDispatcher("personne.jsp").forward(request, response);
				
			} else {
				
				request.getRequestDispatcher("erreur.jsp").forward(request, response);
			}

		} else {

			request.setAttribute("personnes", personnes);
			request.getRequestDispatcher("personnes.jsp").forward(request, response);
			
		}
	
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

	
		if (request.getParameter("nom") != null && request.getParameter("prenom") != null) {

			String nom = request.getParameter("nom");
			String prenom = request.getParameter("prenom");
			Personne personne = new Personne(nom, prenom);

			if (personneService.addPersonne(personne)) {

				request.setAttribute("personnes", personnes);
				request.getRequestDispatcher("personnes.jsp").forward(request, response);

			} else {
				request.getRequestDispatcher("erreur.jsp").forward(request, response);
			}

		}else {
			request.getRequestDispatcher("erreur.jsp").forward(request, response);
		}

	

	}

	
	
	

}
