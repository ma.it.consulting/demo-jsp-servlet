<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Affichage une personne</title>
</head>
<body>

	<div>
		<h1>Formulaire pour les personnes : </h1>

		<form action="personne-demo3" method="post">
			<label>Nom : </label>
			<input type="text" name="nom"/>
			<label>Prenom : </label>
			<input type="text" name="prenom"/>
			<input type="submit" value="validation"/>
			
		</form>


	</div>


	<h1>La liste des personnes :</h1>
	<div>
		<c:forEach items="${personnes}" var="personne">

			<div>${personne.getNom()}</div>

		</c:forEach>

	</div>



</body>
</html>