<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Demo</title>
</head>
<body>
	<h1>Menu principal</h1>
	<br />

	<a href="myPage">MyPage</a>
	<br />
	<br />
	<a href="myHttp">MyHttp</a>
	<br />
	<br />
	<a href="FeedBackServlet">FeedBack</a>
	<br />
	<br />
	<a href="personnes">Personnes</a>
	<br />
	<br />
	<a href="personne-demo2">personne avec id</a>
	<br />
	<br />
	<a href="personne-demo3">personne avec id et avec jsp</a>
</body>
</html>